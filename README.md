<h1 align="center">Weather Station</h1>

<p align="center"><img src="https://img.shields.io/badge/Licence-MIT-green.svg?style=for-the-badge" /></p>

### 项目介绍

整合之前学到的技术新开一个气象站项目，以产品向作为开发目标

用户可以参考 [项目使用说明](https://gitee.com/walkline/WeatherStation/wikis)

### 开发工具

* [Visual Studio Code](https://code.visualstudio.com)
* [RT-Thread MicroPython 插件](https://marketplace.visualstudio.com/items?itemName=RT-Thread.rt-thread-micropython)
* [MicroWebSrv2 模块](https://github.com/jczic/MicroWebSrv2)
* [MicroDNSSrv 模块](https://github.com/jczic/MicroDNSSrv)

### 自定义固件

访问 [ESP32 自定义固件下载](https://gitee.com/walkline/esp32_firmware) 查看和下载最新固件

### 自定义硬件

访问 [Walkline Hardware](https://gitee.com/walkline/walkline-wardware/tree/master/WeatherStation/v0.1) 查看`WeatherStation v0.1`自定义硬件

### 准备材料

* esp32 开发板（刷`自定义固件`）
* oled 屏幕（IIC 接口）
* 4 脚按钮
* dht11 温湿度传感器（OneWire 接口）
* ds18b20 温度传感器（OneWire 接口）
* mqtt 服务器：[扇贝物联](http://bigiot.xyz)
* 字模提取软件：[Dot Matrix Generator](https://gitee.com/walkline/Dot-Matrix-Generator)

### 截图

<p align="center"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/screenshots/screenshot_01.jpg" width="400px" alt="智能配网" /></p>

### 硬件连接

| 硬件版本 v0 | 硬件版本 v1 | 硬件版本 v2 |
| :--------: | :--------: | :--------: |
| <img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/接线图_v0.png" width="400px" alt="接线图v0" /> | <img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/接线图_v1.jpg" width="400px" alt="接线图v1" /> | <img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/接线图_v2.jpg" width="400px" alt="接线图v2" /> |

### 功能规划

* 需要配网功能（[配网视频](https://www.bilibili.com/video/av80421511/)）
    * 使用一个按钮，长按数秒进入配网模式
    * 配网模式开启`ap 热点`，提供 `http web server`，手机端连接热点访问网页进行配网和相关设置
    * 保存设置后生成新的配置文件并测试热点是否连接成功
    * 测试成功后重启设备，进入正常工作模式
* 获取温湿度数据
    * v1 版使用`dht11`
    * v2 版使用`ds18b20`
* oled 显示更新后的温湿度数据
* 通过 mqtt 发布主题，更新温湿度数据到云端

### 功能模块

* [x] `http web server`和配网页面文件
* [x] `websockets service`，用于配网页面和板子之间发布命令交换数据
* [x] `dns server`，使用域名`walkline.wang`访问配网页面
* [x] 测试 wifi 连接
* [x] 测试网络连接
* [x] 测试`mqtt server`参数
* [x] 保存用户设置
* [x] 按钮长按功能
* [x] 指示灯闪烁提示不同状态
* [ ] ...
* [ ] ...

### 额外功能

使用 app 端（小程序、Android 等），通过 mqtt 服务器发布消息，实现远程唤醒局域网电脑（wol）

### 合作及交流

* 联系邮箱：<walkline@163.com>
* QQ 交流群：
    * 走线物联：163271910
    * 扇贝物联：31324057

<p align="center"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_walkline.png" width="300px" alt="走线物联"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_bigiot.png" width="300px" alt="扇贝物联"></p>
