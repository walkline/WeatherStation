from utils import Utilities

"""
接线说明
===============			===============			===============
   v1(无外设)			   v1(带oled屏)					v2
===============			===============			===============

Button: GPIO0			VCC <==> VIN			VCC <==> VIN
						GND <==> GND			GND <==> GND

						OLED:					Button: GPIO27
						SCL <==> GPIO22
						SDA <==> GPIO21			DS18B20: GPIO26

						Button: GPIO13

						DHT11: GPIO04
"""

class Config(object):
	"""
	硬件配置文件
	"""

	"""
	# 硬件版本
	# 由于接线以及元件不同所以使用版本号以示区别
	# 具体区别可以参考这里: https://gitee.com/walkline/WeatherStation
	"""
	VERSION_0 = 0
	VERSION_1 = 1
	VERSION_2 = 2

	# 针对不同的硬件版本选择对应的版本号
	# VERSION_0 为特殊版本，不需要外接任何元件
	HARDWARE_VERSION = VERSION_0

	"""
	# GPIO 设置
	"""
	__BUTTON = [0, 13, 27]
	# BUTTON = __BUTTON_V1 if HARDWARE_VERSION == VERSION_1 else __BUTTON_V2
	# BUTTON = HARDWARE_VERSION == VERSION_1 and __BUTTON_V1 or __BUTTON_V2
	BUTTON = __BUTTON[HARDWARE_VERSION]

	DS18B20_DATALINE = 26
	DHT11_DATALINE = 4

	OLED_SCL = 22
	OLED_SDA = 21

	"""
	# Internet 测试文件设置
	"""
	# 二选一，或者自己指定
	INTERNET_TESTING_URL = "https://walkline.wang/success.html"
	# INTERNET_TESTING_URL = "https://gitee.com/walkline/WeatherStation/raw/master/success.html"

	"""
	# AP 热点设置
	"""
	AP_SSID = "esp_{}".format(Utilities.get_chip_id())
	AP_AUTHMODE = 0
	AP_HOST = "192.168.66.1"
	AP_PORT = 80 # if you modify port number, please set the same value in web/js/index.js
	AP_ROOTPATH = "/web"
	AP_IFCONFIG = (AP_HOST, "255.255.255.0", AP_HOST, AP_HOST)

	"""
	# DNS 设置
	"""
	__CAPTIVE_PORTAL = {"*": AP_HOST}
	__PORTAL_WALKLINE_WANG = {"walkline.wang": AP_HOST}
	__PORTAL_BIGIOT_XYZ = {"bigiot.xyz": AP_HOST}
	__PORTAL_MIXED = {"walkline.wang": AP_HOST, "bigiot.xyz": AP_HOST}

	# 上边为可选的内容，下边为使用的内容
	DNS_PORTAL = __PORTAL_MIXED
