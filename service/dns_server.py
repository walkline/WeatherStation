from microDNSSrv import MicroDNSSrv

class DNSServer(object):
	def __init__(self, domain_list):
		self.__server = MicroDNSSrv()
		self.__server.SetDomainsList(domain_list)
	
	def start(self):
		if self.__server.Start():
			print("MicroDNSSrv started")
		else:
			print("Start MicroDNSSrv failed")

	def stop(self):
		self.__server.Stop()

	def is_running(self):
		return self.__server.IsStarted()
