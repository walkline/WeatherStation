template =\
"""# auto generated file, do not modify
class Settings(object):
	WIFI_SSID = "{wifi_ssid}"
	WIFI_PASSWORD = "{wifi_password}"

	MQTT_HOST = "{mqtt_host}"
	MQTT_PORT = {mqtt_port}
	# MQTT_WS_PORT = 8083
	MQTT_KEEPALIVE = {mqtt_keepalive}
	MQTT_PATH = "{mqtt_path}"
	MQTT_USERNAME = "{mqtt_username}"
	MQTT_DEVICE_NUMBER = "{mqtt_device_number}"
	MQTT_DEVICE_AUTHORIZE = "{mqtt_device_authorize}"
	MQTT_DEVICE_NAME = "{mqtt_device_name}"
	MQTT_DATA_POINT = ({mqtt_data_point},)

"""
