class Utilities(object):
	@staticmethod
	def get_chip_id():
		from machine import unique_id

		return "".join(['%02X' % i for i in unique_id()])

	@classmethod
	def connect_to_internet(cls):
		from utils.wifihandler import WifiHandler

		try:
			from settings import Settings
		except ImportError:
			# 至此说明 mode 文件存在，但是 settings.py 文件不存在
			# 则需要删除 mode 文件并复位，进入配网模式
			cls.enter_smart_config_mode()

		result_code = WifiHandler.set_sta_mode(Settings.WIFI_SSID, Settings.WIFI_PASSWORD)

		return result_code
	
	@classmethod
	def enter_smart_config_mode(cls):
		"""
		进入配网模式
		"""
		cls.del_mode_file()
		cls.hard_reset()

	@staticmethod
	def is_mode_file_exist():
		"""
		暂时规定如果 mode 文件存在，则表示启用 ap 模式，用于用户配网
		"""
		import os

		try:
			os.stat("mode")

			return True
		except OSError:
			return False

	@staticmethod
	def make_mode_file():
		"""
		创建 mode 文件，开启 ap 模式
		"""
		try:
			with open("mode", "w") as file:
				pass

			return True
		except OSError:
			return False

	@staticmethod
	def del_mode_file():
		"""
		删除 mode 文件，开启配网模式
		"""
		import os

		try:
			os.remove("mode")

			return True
		except OSError:
			return False

	@staticmethod
	def soft_reset():
		"""
		a soft reset
		"""

		from sys import exit

		exit()

	@staticmethod
	def hard_reset():
		"""
		a hard reset
		"""

		from machine import reset

		reset()
