'use strict';

if (/Android [4-9]/.test(navigator.appVersion)) {
	window.addEventListener("resize", function() {
		if (document.activeElement.tagName == "INPUT" || document.activeElement.tagName == "TEXTAREA") {
			window.setTimeout(function() {
				document.activeElement.scrollIntoViewIfNeeded();
			}, 500);
		}
	});
}

function isEmpty(obj) {
	if (typeof obj == "undefined" || obj == null || obj == "") {
		return true;
	} else {
		return false;
	}
}

String.prototype.startWith = function (str) {
	var reg = new RegExp("^" + str);
	return reg.test(this);
}  
  
String.prototype.endWith = function (str) {
	var reg = new RegExp(str + "$");
	return reg.test(this);
}